class CasaCheiaException(Exception):
    def __init__(self,val):
        self.val = val

    def __str__(self):
        return repr(self.val)

class Espetaculo:
    def __init__(self, nome, capacidade, custo):
        self.nome = nome
        self.capacidade = capacidade
        self.espetadores = 0
        self.custo = custo

    def __str__(self):
        return "{} com {} para um maximo de {}, com custo de {}".format(self.nome,
                                                                        self.espetadores,
                                                                        self.capacidade,
                                                                        self.custo)

    def setNome(self, nome):
        self.nome = nome

    def getnome(self):
        return self.nome

    def setCapacidade(self,capacidade):
        self.capacidade = capacidade

    def getCapacidade(self):
        return self.capacidade
    # (...)

    def equals(self,obj):
        if(self.nome == obj.nome) and (self.capacidade == obj.capacidade) and (self.espetadores == obj.espetadores) and (self.custo == obj.custo):
            return True
        return False

    def comprarbilhete(self):
        if self.espetadores + 1 > self.capacidade:
            raise CasaCheiaException("Espetaculo Esgotado")
        else:
            self.espetadores +=1
            return "Tem a pagar:{} ({}/{})".format(self.custo, self.espetadores,self.capacidade)


class Historico:
    def __init__(self,ano):
        self.ano = ano
        self.lista = []

    def adicionarEspetaculo(self, espetaculo: Espetaculo):
        self.lista.append(espetaculo)

    def verEsgotados(self):
        aux = 0
        for i in self.lista:
            if i.espetadores == i.capacidade:
                aux += 1
        return "{} Esgotados em {}".format(aux,len(self.lista))

    def calcularReceitas(self):
        aux = 0
        for i in self.lista:
            aux += i.custo * i.espetadores

        return aux


class teste:
    def __init__(self):
        pass

    def maisespetadores(self,historico,nome):
        aux = 0
        for ano in historico:
            for j in ano:
                if j.nome == nome:
                    aux += 1
        return aux
"""
teste = Espetaculo("abc",5,1)


try:
    for i in range(0, 10):
        print(teste.comprarbilhete())
except CasaCheiaException as c:
    print(c.val)

"""
