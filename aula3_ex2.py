from threading import Thread
from multiprocessing import Process
import urllib.request

import time
#ExecuçãoSequencial

def filefunc():
    file = open ("/home/mfernandes/VirtualBox VMs/Aulas/Aulas.vdi", "rb")
    size = 1024
    for i in range (1000):
        file.read(size)

def webrequest():
    for i in range(10):
        with urllib.request.urlopen("https://google.com")as f:
            f.read(1024)
def fibonacci():
    #Fibonacci
    a = 0
    b = 1
    for i in range (100000):
        a = b
        b = a + b




class nothreads_object(object):
    def __init__(self,func):
        self.func = func

    def run(self):
        self.func()


def non_threaded(num_iter,fun):
    funcs = []
    for i in range(int(num_iter)):
        funcs.append(nothreads_object(fun))
    for i in funcs:
        i.run()

class threads_object(Thread):
    def __init__(self,func):
        Thread.__init__(self)
        self.func = func
    def run(self):
        self.func()


class multiproc_object(Process):
    def __init__(self,func):
        Process.__init__(self)
        self.func = func
    def run(self):
        self.func()


def threaded(num_threads,func):
    funcs = []
    for i in range(int(num_threads)):
        funcs.append(threads_object(func))
    for i in funcs:
        i.start()
    for i in funcs:
        i.join()


def multiprocess(num_proc,func):
    procs =[]
    for i in range(int(num_proc)):
        procs.append(multiproc_object(func))
    for i in procs:
        i.start()
    for i in procs:
        i.join()

def show_results(func_name, results,name):
    print ("%-23s %4.6f seconds (%s)"% (func_name, results,name))

if __name__ == "__main__":
    funcoes = []
    funcoes.append(fibonacci)
    funcoes.append(webrequest)
    funcoes.append(filefunc)
    num_threads = [ 1, 2, 4, 8, 16 ]
    print('Starting tests')
    for fun,name in zip(funcoes,["fib","web","file"]):
        print(name)
        for i in num_threads:
            start = time.time()
            non_threaded(i,fun)
            executionTime = time.time() - start
            show_results("non_threaded (%s iters) " % i, executionTime,name)
            start = time.time()
            threaded (i,fun)
            executionTime = time.time() - start
            show_results("threaded (%s threads)" % i, executionTime,name)
            start = time.time()
            multiprocess(i,fun)
            executionTime = time.time() - start
            show_results("MultiProc (%s Procs)" % i, executionTime,name)

    print ('Iterations complete')