from mpi4py import MPI
import numpy as np
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
size = comm.Get_size()
a_size = 4
recvdata = None
senddata = (rank+1)*np.arange(a_size, dtype=np.float64)
if rank == 0:
    recvdata = np.arange(size*a_size,dtype=np.float64)
comm.Gather(senddata,recvdata,root=0)
print ("on process %s, after Gather data = %s" % (rank, recvdata ))