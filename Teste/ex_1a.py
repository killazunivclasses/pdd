import numpy as np
import time


def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("Somatorio:{} , timestamp:{}".format(sum(pos_impares),time.ctime()))


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
soma_pos_impares(x,len(x))