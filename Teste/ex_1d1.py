from mpi4py import MPI
import numpy as np
import time

comm = MPI.COMM_WORLD
nprocs = comm.Get_size()
rank = comm.Get_rank()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("Rank:{}, Somatorio:{} , timestamp:{}".format(rank,sum(pos_impares),time.ctime()))
    return sum(pos_impares)


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
dim = 8414
scatter_list = []
if rank == 0:
    for cnt in range(nprocs):
        init = (cnt * dim) // nprocs
        end = (((cnt + 1) * dim) // nprocs)
        # print(init,end)
        newlist = x[init:end]
        scatter_list.append(newlist)
        #soma_pos_impares(newlist,len(newlist))
    data = None
else:
    data = None

data = comm.scatter(scatter_list, root=0)
out = soma_pos_impares(data,len(data))
newdata = comm.gather(out,root=0)

if rank == 0:
    print("Parciais:{}".format(newdata))
    print("Total:{}".format(sum(newdata)))



