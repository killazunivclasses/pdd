import numpy as np
import time
import os
from multiprocessing import Process, Queue
from queue import Empty

from PyQt5.QtCore import QAbstractAnimation

proc_queue = Queue()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("PID:{}, Somatorio:{} , timestamp:{}".format(os.getpid(),sum(pos_impares),time.ctime()))
    proc_queue.put(sum(pos_impares))

x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
#soma_pos_impares(x,len(x))

dim = 8414
q = -1
#q = 7 # (2,7,14)
"""
Os valores validos foram obtidos por:

for i in range(1,100):
    if(dim % i == 0):
        print(i)
"""
while(q == -1):
    q = int(input("Numero de processos? Tem de ser divisor de 8414 (1,2,7,14):"))
    if not dim % q == 0:
        q = -1

arr_procs = []
auxcnt = 0
for cnt in range(q):
    init = (cnt * dim) // q
    end = (((cnt+1) * dim) // q)
    # print(init,end)
    newlist = x[init:end]
    arr_procs.append(Process(target=soma_pos_impares, args=(newlist, len(newlist))))
for prc in arr_procs:
    prc.start()

for prc in arr_procs:
    prc.join()

partial_sums = []
while True:
    try:
        partial_sums.append(proc_queue.get(False))
    except Empty:
        break

print("Parciais:{}".format(partial_sums))
print("Total:{}".format(sum(partial_sums)))
