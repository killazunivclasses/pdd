# Miguel Fernandes, M8414
# a )
import numpy as np
import time


def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("Somatorio:{} , timestamp:{}".format(sum(pos_impares),time.ctime()))


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
soma_pos_impares(x,len(x))
####
# Output
####
# /usr/bin/python3.6 /home/mfernandes/PycharmProjects/pdd/Teste/ex_1a.py
# Somatorio:208320 , timestamp:Thu May 10 16:28:29 2018

# b )
import numpy as np
import time
import threading

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("{}, Somatorio:{} , timestamp:{}".format(threading.current_thread().getName(),sum(pos_impares),time.ctime()))


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
dim = 8414
q = -1
#q = 7 # (2,7,14)
"""
Os valores validos foram obtidos por:

for i in range(1,100):
    if(dim % i == 0):
        print(i)
"""
while(q == -1):
    q = int(input("Numero de threads? Tem de ser divisor de 8414 (1,2,7,14):"))
    if not dim % q == 0:
        q = -1

arr_threads = []
auxcnt = 0
for cnt in range(q):
    init = (cnt * dim) // q
    end = (((cnt+1) * dim) // q)
    # print(init,end)
    newlist = x[init:end]
    arr_threads.append(threading.Thread(target=soma_pos_impares, args=(newlist, len(newlist))))
for th in arr_threads:
    th.start()

####
# Output
####
#/usr/bin/python3.6 /home/mfernandes/PycharmProjects/pdd/Teste/ex_1b.py
#Numero de threads? Tem de ser divisor de 8414 (1,2,7,14):7
#Thread-1, Somatorio:29641 , timestamp:Thu May 10 16:51:55 2018
#Thread-2, Somatorio:28852 , timestamp:Thu May 10 16:51:55 2018
#Thread-3, Somatorio:29445 , timestamp:Thu May 10 16:51:55 2018
#Thread-4, Somatorio:29198 , timestamp:Thu May 10 16:51:55 2018
#Thread-5, Somatorio:28930 , timestamp:Thu May 10 16:51:55 2018
#Thread-6, Somatorio:29016 , timestamp:Thu May 10 16:51:55 2018
#Thread-7, Somatorio:29113 , timestamp:Thu May 10 16:51:55 2018
#
#Process finished with exit code 0

# c )
import numpy as np
import time
import os
from multiprocessing import Process

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("PID:{}, Somatorio:{} , timestamp:{}".format(os.getpid(),sum(pos_impares),time.ctime()))


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
#soma_pos_impares(x,len(x))

dim = 8414
q = -1
#q = 7 # (2,7,14)
"""
Os valores validos foram obtidos por:

for i in range(1,100):
    if(dim % i == 0):
        print(i)
"""
while(q == -1):
    q = int(input("Numero de processos? Tem de ser divisor de 8414 (1,2,7,14):"))
    if not dim % q == 0:
        q = -1

arr_procs = []
auxcnt = 0
for cnt in range(q):
    init = (cnt * dim) // q
    end = (((cnt+1) * dim) // q)
    # print(init,end)
    newlist = x[init:end]
    arr_procs.append(Process(target=soma_pos_impares, args=(newlist, len(newlist))))
for prc in arr_procs:
    prc.start()

####
# Output
####
#/usr/bin/python3.6 /home/mfernandes/PycharmProjects/pdd/Teste/ex_1c.py
#Numero de processos? Tem de ser divisor de 8414 (1,2,7,14):7
#PID:3918, Somatorio:30605 , timestamp:Thu May 10 16:58:46 2018
#PID:3919, Somatorio:29568 , timestamp:Thu May 10 16:58:46 2018
#PID:3920, Somatorio:30286 , timestamp:Thu May 10 16:58:46 2018
#PID:3922, Somatorio:29424 , timestamp:Thu May 10 16:58:46 2018
#PID:3921, Somatorio:30441 , timestamp:Thu May 10 16:58:46 2018
#PID:3923, Somatorio:30121 , timestamp:Thu May 10 16:58:46 2018
#PID:3924, Somatorio:29238 , timestamp:Thu May 10 16:58:46 2018
#
#Process finished with exit code 0


# d )
from mpi4py import MPI
import numpy as np
import time

comm = MPI.COMM_WORLD
nprocs = comm.Get_size()
rank = comm.Get_rank()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("Rank:{}, Somatorio:{} , timestamp:{}".format(rank,sum(pos_impares),time.ctime()))


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
dim = 8414
scatter_list = []
if rank == 0:
    for cnt in range(nprocs):
        init = (cnt * dim) // nprocs
        end = (((cnt + 1) * dim) // nprocs)
        # print(init,end)
        newlist = x[init:end]
        scatter_list.append(newlist)
        #soma_pos_impares(newlist,len(newlist))
    data = None
else:
    data = None

data = comm.scatter(scatter_list, root=0)
soma_pos_impares(data,len(data))


####
# Output
####
#

#->mpiexec -n 10 python ex_1d.py
#Rank:1, Somatorio:20388 , timestamp:Thu May 10 17:17:44 2018
#Rank:2, Somatorio:21118 , timestamp:Thu May 10 17:17:44 2018
#Rank:3, Somatorio:20726 , timestamp:Thu May 10 17:17:44 2018
#Rank:4, Somatorio:21254 , timestamp:Thu May 10 17:17:44 2018
#Rank:5, Somatorio:20813 , timestamp:Thu May 10 17:17:44 2018
#Rank:6, Somatorio:20386 , timestamp:Thu May 10 17:17:44 2018
#Rank:7, Somatorio:21073 , timestamp:Thu May 10 17:17:44 2018
#Rank:8, Somatorio:19241 , timestamp:Thu May 10 17:17:44 2018
#Rank:0, Somatorio:20345 , timestamp:Thu May 10 17:17:44 2018
#Rank:9, Somatorio:20611 , timestamp:Thu May 10 17:17:44 2018
#

# b1 )

import numpy as np
import time
import threading
import queue

th_queue = queue.Queue()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("{}, Somatorio:{} , timestamp:{}".format(threading.current_thread().getName(),sum(pos_impares),time.ctime()))
    th_queue.put(sum(pos_impares))

x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
#soma_pos_impares(x,len(x))

dim = 8414
q = -1
#q = 7 # (2,7,14)
"""
Os valores validos foram obtidos por:

for i in range(1,100):
    if(dim % i == 0):
        print(i)
"""
while(q == -1):
    q = int(input("Numero de threads? Tem de ser divisor de 8414 (1,2,7,14):"))
    if not dim % q == 0:
        q = -1

arr_threads = []
auxcnt = 0
for cnt in range(q):
    init = (cnt * dim) // q
    end = (((cnt+1) * dim) // q)
    # print(init,end)
    newlist = x[init:end]
    arr_threads.append(threading.Thread(target=soma_pos_impares, args=(newlist, len(newlist))))
for th in arr_threads:
    th.start()

for th in arr_threads:
    th.join()

partial_sums = []
while True:
    try:
        partial_sums.append(th_queue.get(False))
    except queue.Empty:
        break

print("Parciais:{}".format(partial_sums))
print("Total:{}".format(sum(partial_sums)))



####
# Output
####

#/usr/bin/python3.6 /home/mfernandes/PycharmProjects/pdd/Teste/ex_1b.py
#Numero de threads? Tem de ser divisor de 8414 (1,2,7,14):7
#Thread-1, Somatorio:28977 , timestamp:Thu May 10 17:30:57 2018
#Thread-2, Somatorio:30636 , timestamp:Thu May 10 17:30:57 2018
#Thread-3, Somatorio:29889 , timestamp:Thu May 10 17:30:57 2018
#Thread-4, Somatorio:29492 , timestamp:Thu May 10 17:30:57 2018
#Thread-5, Somatorio:28931 , timestamp:Thu May 10 17:30:57 2018
#Thread-6, Somatorio:29279 , timestamp:Thu May 10 17:30:57 2018
#Thread-7, Somatorio:30437 , timestamp:Thu May 10 17:30:57 2018
#Parciais:[28977, 30636, 29889, 29492, 28931, 29279, 30437]
#Total:207641
#
#Process finished with exit code 0

# c1 )

import numpy as np
import time
import os
from multiprocessing import Process, Queue
from queue import Empty

from PyQt5.QtCore import QAbstractAnimation

proc_queue = Queue()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("PID:{}, Somatorio:{} , timestamp:{}".format(os.getpid(),sum(pos_impares),time.ctime()))
    proc_queue.put(sum(pos_impares))

x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
#soma_pos_impares(x,len(x))

dim = 8414
q = -1
#q = 7 # (2,7,14)
"""
Os valores validos foram obtidos por:

for i in range(1,100):
    if(dim % i == 0):
        print(i)
"""
while(q == -1):
    q = int(input("Numero de processos? Tem de ser divisor de 8414 (1,2,7,14):"))
    if not dim % q == 0:
        q = -1

arr_procs = []
auxcnt = 0
for cnt in range(q):
    init = (cnt * dim) // q
    end = (((cnt+1) * dim) // q)
    # print(init,end)
    newlist = x[init:end]
    arr_procs.append(Process(target=soma_pos_impares, args=(newlist, len(newlist))))
for prc in arr_procs:
    prc.start()

for prc in arr_procs:
    prc.join()

partial_sums = []
while True:
    try:
        partial_sums.append(proc_queue.get(False))
    except Empty:
        break

print("Parciais:{}".format(partial_sums))
print("Total:{}".format(sum(partial_sums)))


####
# Output
####

#/usr/bin/python3.6 /home/mfernandes/PycharmProjects/pdd/Teste/ex_1c.py
#Numero de processos? Tem de ser divisor de 8414 (1,2,7,14):7
#PID:5966, Somatorio:30658 , timestamp:Thu May 10 17:37:09 2018
#PID:5967, Somatorio:29997 , timestamp:Thu May 10 17:37:09 2018
#PID:5969, Somatorio:29322 , timestamp:Thu May 10 17:37:09 2018
#PID:5971, Somatorio:29580 , timestamp:Thu May 10 17:37:09 2018
#PID:5975, Somatorio:29842 , timestamp:Thu May 10 17:37:09 2018
#PID:5972, Somatorio:30440 , timestamp:Thu May 10 17:37:09 2018
#PID:5976, Somatorio:29117 , timestamp:Thu May 10 17:37:09 2018
#Parciais:[30658, 29997, 29322, 29580, 30440, 29842, 29117]
#Total:208956
#
#Process finished with exit code 0


# d1 )

from mpi4py import MPI
import numpy as np
import time

comm = MPI.COMM_WORLD
nprocs = comm.Get_size()
rank = comm.Get_rank()

def soma_pos_impares(arr,lenn):
    pos_impares = []
    for i in range(1, lenn, 2):
        pos_impares.append(arr[i])
    print("Rank:{}, Somatorio:{} , timestamp:{}".format(rank,sum(pos_impares),time.ctime()))
    return sum(pos_impares)


x = np.random.randint(100,size=8414) # gera valores aleatorios entre 0 e 100
dim = 8414
scatter_list = []
if rank == 0:
    for cnt in range(nprocs):
        init = (cnt * dim) // nprocs
        end = (((cnt + 1) * dim) // nprocs)
        # print(init,end)
        newlist = x[init:end]
        scatter_list.append(newlist)
        #soma_pos_impares(newlist,len(newlist))
    data = None
else:
    data = None

data = comm.scatter(scatter_list, root=0)
out = soma_pos_impares(data,len(data))
newdata = comm.gather(out,root=0)

if rank == 0:
    print("Parciais:{}".format(newdata))
    print("Total:{}".format(sum(newdata)))

####
# Output
####
#->mpiexec -n 10 python ex_1d1.py
#Rank:1, Somatorio:20318 , timestamp:Thu May 10 17:21:50 2018
#Rank:2, Somatorio:21160 , timestamp:Thu May 10 17:21:50 2018
#Rank:3, Somatorio:19671 , timestamp:Thu May 10 17:21:50 2018
#Rank:4, Somatorio:20920 , timestamp:Thu May 10 17:21:50 2018
#Rank:5, Somatorio:20233 , timestamp:Thu May 10 17:21:50 2018
#Rank:6, Somatorio:21356 , timestamp:Thu May 10 17:21:50 2018
#Rank:7, Somatorio:20918 , timestamp:Thu May 10 17:21:50 2018
#Rank:8, Somatorio:20152 , timestamp:Thu May 10 17:21:50 2018
#Rank:0, Somatorio:21366 , timestamp:Thu May 10 17:21:50 2018
#Rank:9, Somatorio:20373 , timestamp:Thu May 10 17:21:50 2018
#Parciais:[21366, 20318, 21160, 19671, 20920, 20233, 21356, 20918, 20152, 20373]
#Total:206467
#