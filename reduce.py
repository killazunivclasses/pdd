import numpy as np
from mpi4py import MPI
comm = MPI.COMM_WORLD
size = comm.size
rank = comm.rank
a_size = 3
recvdata = np.zeros(a_size,dtype=np.int)
senddata = (rank+1)*np.arange(a_size, dtype=np.int)
print(" process %s sending %s " %(rank , senddata))
comm.Reduce(senddata,recvdata,root=0,op=MPI.SUM)
print ('on task',rank,'after Reduce: data = ',recvdata)