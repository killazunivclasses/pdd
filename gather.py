from mpi4py import MPI
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
data1 = (rank+1)**2
data = comm.gather(data1, root=0)
if rank == 0:
    print ("rank = %s " %rank + "...receiving data from other process")
    print (data)