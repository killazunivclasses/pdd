3
#Thread que no servidor comunica com o cliente:
import socket, threading

class ClientThread(threading.Thread):
    def __init__(self,clientAddress,clientsocket):
        threading.Thread.__init__(self)
        self.clientAddress = clientAddress
        self.csocket = clientsocket
        print ("New connection added: ", clientAddress)

    def run(self):
        global premio
        print ("Connection from : ", self.clientAddress)
        msg = ''
        while True:
            data = self.csocket.recv(2048)
            msg = data.decode()
            if msg=='bye':
                break
            print ("from client", msg)
            self.csocket.send(bytes("Premio={}".format(premio),'UTF-8'))
            premio +=1
        print ("Client at ", self.clientAddress , " disconnected...")


LOCALHOST = "127.0.0.1"
PORT = 2000
premio = 0
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((LOCALHOST, PORT))
print("Server started")
print("Waiting for client request..")
while True:
    server.listen(1)
    clientsock, clientAddress = server.accept()
    newthread = ClientThread(clientAddress, clientsock)
    newthread.start()
