import threading
import random
import time
class Philosopher(threading.Thread):
    running = True # variável de classe
    def __init__(self, name, forkOnLeft, forkOnRight):
        threading.Thread.__init__(self)
        self.name = name
        self.forkOnLeft = forkOnLeft
        self.forkOnRight = forkOnRight

    def run(self):
        while(self.running):
            # Philosopher is thinking.
            print("")
            time.sleep(1)
            print ('%s is hungry.' % self.name)
            self.dine()

    def dine(self):
        self.forkOnLeft.acquire()
        self.forkOnRight.acquire()
        self.dining()
        self.forkOnLeft.release()
        self.forkOnRight.release()

    def dine2(self):
        fork1, fork2 = self.forkOnLeft, self.forkOnRight
        while self.running:
            fork1.acquire(True)
            locked = fork2.acquire(False)
            if locked:
                break
            fork1.release()
            print('%s swaps forks' % self.name)
            fork1, fork2 = fork2, fork1
        else:
            return
        self.dining()
        fork2.release()
        fork1.release()

    def dining(self):
        print ('%s starts eating '% self.name)
        time.sleep(1)
        print ('%s finishes eating and leaves to think.' % self.name)

def DiningPhilosophers():
    print("Init")
    forks = [threading.Lock() for n in range(5)]
    philosopherNames = ('Aristotle','Kant','Buddha','Marx', 'Russel')
    philosophers= [Philosopher(philosopherNames[i], forks[i%5],forks[(i+1)%5]) for i in range(5)]
    random.seed(507129)
    Philosopher.running = True
    for p in philosophers: p.start()
    time.sleep(30)
    Philosopher.running = False
    print(" ***Now we're finishing.")


DiningPhilosophers()
