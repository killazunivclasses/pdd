import time
import threading
import random

class ContaBancaria:
    def __init__ (self):
        self.saldo = 0
        self.historico = []
        self.historico.append("Inicio.")

    def ver_saldo(self):
        return "Saldo:{:.2f}".format(self.saldo)

    def deposito(self,valor,disc):
        self.saldo += valor
        self.historico.append("Deposito:{:.2f}, {}".format(valor,disc))

    def levantamento(self,valor,disc):
        if self.saldo - valor > 0:
            self.saldo -= valor
            self.historico.append("Levantamento:{:.2f}, {}".format(valor,disc))


class Financiador(threading.Thread):
    def __init__(self, cb):
        threading.Thread.__init__(self)
        self.ContaBancaria = cb

    def run(self):
        while True:
            self.ContaBancaria.deposito(random.uniform(1,1000),self.name)
            time.sleep(1)


class Utilizador(threading.Thread):
    def __init__(self,cb):
        threading.Thread.__init__(self)
        self.ContaBancaria = cb

    def run(self):
        while True:
            self.ContaBancaria.levantamento(random.uniform(1,1000),self.name)
            time.sleep(0.1)



if __name__ == "__main__":
    cb = ContaBancaria()
    financiadores = []
    utilizadores = []

    for i in range(1):
        financiadores.append(Financiador(cb))
    for i in range(3):
        utilizadores.append(Utilizador(cb))

    for i in financiadores:
        i.start()
    for i in utilizadores:
        i.start()

    while True:
        for i in cb.historico:
            print(i)
        print(cb.ver_saldo())
        print("$#$#$#$#$#$$#$#$#")
        time.sleep(2)



