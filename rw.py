import time
import threading
import random
"""
a) Construa uma classe em RW, que possua um campo inteiro, XPTO, e
dois métodos: ler e escrever. O método ler deve devolver o valor da
variável XPTO; o método escrever deve adicionar o valor 100 à
variável XPTO e seguidamente subtrair o mesmo valor à variável XPTO.

b) Pretende-se que um objecto da classe RW seja partilhado por
vários processos (Threads) de dois tipos:
- processos Leitores – que lêem o valor da variável XPTO usando o
método ler;
- processos Escritores – que alteram a variável XPTO usando o
método escrever.
- Construa as classes Leitor e Escritor. Cada uma destas classes deve
ter uma Thread de execução própria em que, num ciclo infinito, vão
respectivamente lendo e alterando valores do objecto partilhado

c) Construa uma classe de teste que crie um objecto do tipo RW, 3
objectos do tipo Leitor e 2 objectos do tipo Escritor. Estude o
comportamento do seu programa.
"""


class RW:

    def __init__(self):
        self.xpto = 0
        self.loc_lock = threading.Condition()
        self.n_leitores = 0

    def antes_ler(self):
        self.loc_lock.acquire()
        self.n_leitores +=1
        self.loc_lock.release()

    def depois_ler(self):
        self.loc_lock.acquire()
        self.n_leitores -= 1
        if self.n_leitores == 0:
            self.loc_lock.notifyAll()
        self.loc_lock.release()

    def ler(self):
        return self.xpto

    def escrever(self,id):
        #print("INEscritorID:{}, var={}".format(id, self.xpto))
        self.loc_lock.acquire(True)
        while self.n_leitores > 0:
            self.loc_lock.wait()
        self.xpto += 100
        time.sleep(random.uniform(1, 5))
        self.xpto -= 100
        self.loc_lock.release()
        #print("OTEscritorID:{}, var={}".format(id, self.xpto))


class Escritor(threading.Thread):
    def __init__(self, rwriter, id):
        threading.Thread.__init__(self)
        self.rw = rwriter
        self.id = id

    def run(self):
        while True:
            self.rw.escrever(self.id)
            time.sleep(random.uniform(1, 5))


class Leitor(threading.Thread):
    def __init__(self, rwriter, id):
        threading.Thread.__init__(self)
        self.rw = rwriter
        self.id = id

    def run(self):
        while True:
            #time.sleep(0.1)
            self.rw.antes_ler()
            x = self.rw.ler()
            print("LeitorID:{}, var={}".format(self.id,x))
            self.rw.depois_ler()


if __name__ == "__main__":
    print("Init")
    rw = RW()

    nleitores = 2
    leitores = []
    for i in range(nleitores):
        leitores.append(Leitor(rw,i))

    escritores = []
    nescritores = 10
    for i in range(nescritores):
        escritores.append(Escritor(rw,i))

    for i in leitores:
        i.start()
    for i in escritores:
        i.start()
