import requests
from queue import Queue, Empty
from threading import Thread
WEBSITE_LIST = [
'http://envanto.com',
'http://amazon.co.uk',
'http://amazon.com',
'http://facebook.com',
'http://google.com',
'http://google.fr',
'http://google.es',
'http://bing.com',
'http://google.co.uk',
'http://internet.org',
'http://gmail.com',
'http://stackoverflow.com',
'http://github.com',
'http://shopyfy.com',
'http://instagram.com',
'http://youtube.com',
]



def check_website (address, timeout = 5):
    try:
        response = requests.head(address , timeout = timeout)
        if response.status_code >= 400:
            raise Exception ()
        else:
            print(address, "is alive!")
    except Exception:
        print ( "Error: " + str (address) + " returns code: " + str(response.status_code) )

queue = Queue()
for i in WEBSITE_LIST:
    queue.put(i)


class worker(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            try:
                val = queue.get(timeout=1)
            except Empty:
                print("its empty!")
                return
            check_website(val)

workers = []
for i in range(4):
    workers.append(worker())

for i in workers:
    i.start()
    i.join()




