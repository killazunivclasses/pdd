from queue import Queue , Empty
import time
import random
from threading import Thread


class Producer(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        for i in range(10):
            item = random.randint(0, 256)
            self.queue.put(item)
            print('Producer notify : item N° %d appended to queue by %s\n' % (item, self.name))
        time.sleep(1)


class Consumer(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            try:
                item = self.queue.get(False)
                print ('Consumer notify : %d popped from queue by %s' % (item, self.name))
                self.queue.task_done()

            except Empty:
                print("{} timed out".format(self.name))
                return





if __name__ == '__main__':
    queue = Queue()
    t1 = Producer(queue)
    t2 = Consumer(queue)
    t3 = Consumer(queue)
    t1.start()
    t2.start()
    t3.start()
    t1.join()
    t2.join()
    t3.join()